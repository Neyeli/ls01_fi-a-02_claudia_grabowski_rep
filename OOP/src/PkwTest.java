

public class PkwTest {
	
	public static void checkEquality(String variable, String a, String b) {
		System.out.println("Teste: " + variable);
		
		if (a == b) {
			System.out.println(a + " == " + b);
		} else {
			System.out.println(a + " != " + b);
		}
	}
	
	public static void checkEquality(String variable, int a, int b) {
		System.out.println("Teste: " + variable);
		
		if (a == b) {
			System.out.println(a + " == " + b);
		} else {
			System.out.println(a + " != " + b);
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Pkw pwk1 = new Pkw("Corolla", "blau", "hell", 3);
		//Pkw pwk2 = new Pkw("Ente", "rot", "schrill", 1);
		
		//Teste setter and getter
		Pkw pkw1 = new Pkw();
		String pkw1Modell = "Corolla";
		String pkw1Farbe = "blau";
		String pkw1HupenKlang = "hell";
		int pkw1Dauer = 3;		
				
		pkw1.setModell(pkw1Modell);
		pkw1.setFarbe(pkw1Farbe);
		pkw1.setHupenKlang(pkw1HupenKlang);
		pkw1.setDauer(pkw1Dauer);
		
		checkEquality("modell", pkw1Modell, pkw1.getModell());
		checkEquality("farbe", pkw1Farbe, pkw1.getFarbe());
		checkEquality("hupenKlang", pkw1HupenKlang, pkw1.getHupenKlang());
		checkEquality("dauer", pkw1Dauer, pkw1.getDauer());
		
		//Teste Konstuktor
		String pkw2Modell = "Ente";
		String pkw2Farbe = "rot";
		String pkw2HupenKlang = "schrill";
		int pkw2Dauer = 1;
		
		Pkw pkw2 = new Pkw(pkw2Modell, pkw2Farbe, pkw2HupenKlang, pkw2Dauer);
		
		checkEquality("modell", pkw2Modell, pkw2.getModell());
		checkEquality("farbe", pkw2Farbe, pkw2.getFarbe());
		checkEquality("hupenKlang", pkw2HupenKlang, pkw2.getHupenKlang());
		checkEquality("dauer", pkw2Dauer, pkw2.getDauer());
		
		//Teste Geschwindigkeit und Geschwindigkeitslimit für Enten
		int geschwindigkeit1 = 100;
		int geschwindigkeit2 = 200;
		
		pkw1.fahre(geschwindigkeit1);
		
		checkEquality("Geschwindigkeit", pkw1.getMomentaneGeschwindigkeit(), geschwindigkeit1);
		
		pkw2.fahre(geschwindigkeit2);
		
		// Maximale Geschwindigkeit für Ente laut Aufgabenstellung = 179 km/h (Interpretation von darf nicht 180 km/h fahren)
		checkEquality("Geschwindigkeit für Ente", pkw2.getMomentaneGeschwindigkeit(), 179); 
	}
}
