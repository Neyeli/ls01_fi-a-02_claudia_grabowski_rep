public class NutzerTest {

	public static void checkEquality(String variable, String a, String b) {
		System.out.println("Teste: " + variable);
		
		if (a == b) {
			System.out.println(a + " == " + b);
		} else {
			System.out.println(a + " != " + b);
		}
	}
	
	public static void checkEquality(String variable, int a, int b) {
		System.out.println("Teste: " + variable);
		
		if (a == b) {
			System.out.println(a + " == " + b);
		} else {
			System.out.println(a + " != " + b);
		}
	}
	
	public static void checkEquality(String variable, long a, long b) {
		System.out.println("Teste: " + variable);
		
		if (a == b) {
			System.out.println(a + " == " + b);
		} else {
			System.out.println(a + " != " + b);
		}
	}
	
	public static void checkEquality(String variable, boolean a, boolean b) {
		System.out.println("Teste: " + variable);
		
		if (a == b) {
			System.out.println(a + " == " + b);
		} else {
			System.out.println(a + " != " + b);
		}
	}

    public static void main(String[] args) {
    	
    	String nutzername = "test";
        String email = "abc@def.de";
        String passwort = "xyz";
        String geburtstag = "01.01.2000";
        String address = "Müllerstaße 1, 11111 irgendwo";
        boolean isAdmin = false;
        long lastLogin = System.currentTimeMillis();

        Nutzer nutzer = new Nutzer();

        nutzer.setNutzername(nutzername);
        nutzer.setEmail(email);
        nutzer.setPasswort(passwort);
        nutzer.setGeburtstag(geburtstag);
        nutzer.setAdresse(address);
        nutzer.setAdmin(isAdmin);
        nutzer.setLastLogin(lastLogin);

        checkEquality("nutzername", nutzername, nutzer.getNutzername());
        checkEquality("email", email, nutzer.getEmail());
        checkEquality("passwort", passwort, nutzer.getPasswort());
        checkEquality("geburtstag", geburtstag, nutzer.getGeburtstag());
        checkEquality("adress", address, nutzer.getAdresse());
        checkEquality("isAdmin", isAdmin, nutzer.isAdmin());
        checkEquality("lastLogin", lastLogin, nutzer.getLastLogin());
    }
}
