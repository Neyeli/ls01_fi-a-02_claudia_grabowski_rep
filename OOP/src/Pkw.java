
public class Pkw {

	private String modell;
	private String farbe;
	private String hupenKlang;
	private int dauer;
	
	private int momentaneGeschwindigkeit;
	
	public Pkw() {}

	public Pkw(String modell) {
		this.modell = modell;
	}
	
	public Pkw(String modell, String farbe, String hupenKlang, int dauer) {
		this.modell = modell;
		this.farbe = farbe;
		this.hupenKlang = hupenKlang;
		this.dauer = dauer;
	}
	
	public String getModell() {
		return modell;
	}

	public void setModell(String modell) {
		this.modell = modell;
	}

	public String getFarbe() {
		return farbe;
	}

	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}

	public String getHupenKlang() {
		return hupenKlang;
	}

	public void setHupenKlang(String hupenKlang) {
		this.hupenKlang = hupenKlang;
	}

	public int getDauer() {
		return dauer;
	}

	public void setDauer(int dauer) {
		this.dauer = dauer;
	}
	
	public int getMomentaneGeschwindigkeit() {
		return momentaneGeschwindigkeit;
	}

	void fahre(int geschwindigkeit) {
		this.momentaneGeschwindigkeit = geschwindigkeit;
		
		if (this.getModell() == "Ente" && geschwindigkeit > 179) {
			this.momentaneGeschwindigkeit = 179;
		}
	}
	
	private void hupe(String hupenKlang, int dauer) {
		setHupenKlang(hupenKlang);
		setDauer(dauer);
	}
	
	public static void main(String[] args) {
		
	}

}
