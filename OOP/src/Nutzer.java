public class Nutzer {
	
	 	private String nutzername;
	    private String email;
	    private String passwort;
	    private String geburtstag;
	    private String adresse;
	    private boolean isAdmin;
	    private long lastLogin;

	    public Nutzer() {
	    }

	    public Nutzer(String nutzername, String email, String passwort, String geburtstag, String adresse, boolean isAdmin, long lastLogin) {
	        this.nutzername = nutzername;
	        this.email = email;
	        this.passwort = passwort;
	        this.geburtstag = geburtstag;
	        this.adresse = adresse;
	        this.isAdmin = isAdmin;
	        this.lastLogin = lastLogin;
	    }

	    public String getNutzername() {
	        return nutzername;
	    }

	    public void setNutzername(String nutzername) {
	        this.nutzername = nutzername;
	    }

	    public String getEmail() {
	        return email;
	    }

	    public void setEmail(String email) {
	        this.email = email;
	    }

	    public String getPasswort() {
	        return passwort;
	    }

	    public void setPasswort(String passwort) {
	        this.passwort = passwort;
	    }

	    public String getGeburtstag() {
	        return geburtstag;
	    }

	    public void setGeburtstag(String geburtstag) {
	        this.geburtstag = geburtstag;
	    }

	    public String getAdresse() {
	        return adresse;
	    }

	    public void setAdresse(String adresse) {
	        this.adresse = adresse;
	    }

	    public boolean isAdmin() {
	        return isAdmin;
	    }

	    public void setAdmin(boolean isAdmin) {
	        this.isAdmin = isAdmin;
	    }

	    public long getLastLogin() {
	        return lastLogin;
	    }

	    public void setLastLogin(long lastLogin) {
	        this.lastLogin = lastLogin;
	    }

	public static void main(String[] args) {

	}

}
