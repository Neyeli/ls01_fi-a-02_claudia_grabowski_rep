/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author Claudia Grabowski
    @version
*/
public class Variablen {
  public static void main(String [] args){
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */
    int count;

    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/
    count = 25;
    System.out.println("Program count: " + count);

    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */
    char menuSelect;

    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
    menuSelect = 'C';
    System.out.println("Selected menu: " + menuSelect);

    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
    long speedOfLight;

    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
    speedOfLight = 299792458;
    System.out.println("Speed of Light: " + speedOfLight + " m/s");

    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
    byte memberCount = 7;

    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
    System.out.println("Member count: " + memberCount);

    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
    double elementaryCharge = 0.00000000000000000016021766;
    System.out.println("Elementary charge: " + elementaryCharge + " C");

    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
    boolean paymentFulfilled;

    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
    paymentFulfilled = true;
    System.out.println("Payment fulfilled: " + paymentFulfilled);
  }
}
