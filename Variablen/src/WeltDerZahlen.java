/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Claudia Grabowski >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    byte anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstraße
    long anzahlSterne = 300000000000L;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3769000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    short alterTage = 9125;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 200000;
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
    int flaecheGroessteLand = 17098242;
    
    // Wie groß ist das kleinste Land der Erde?
    float flaecheKleinsteLand = 0.44F;
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten in der Milchstraße: " + anzahlPlaneten);
    System.out.println("Anzahl der Sterne in der Milchstraße: " + anzahlSterne);
    System.out.println("Anzahl der Einwohner in Berlin: " + bewohnerBerlin);
    System.out.println("Anzahl meiner gelebten Tage: " + alterTage);
    System.out.println("Maximales Gewicht eines Blauwals in kg: " + gewichtKilogramm);
    System.out.println("Fläche von Russland in km²: " + flaecheGroessteLand);
    System.out.println("Fläche vom Staat Vatikanstadt in km²: " + flaecheKleinsteLand);
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

