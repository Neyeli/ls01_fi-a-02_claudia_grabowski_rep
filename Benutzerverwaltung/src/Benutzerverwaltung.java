import java.util.Scanner;
import java.util.Arrays;
import java.time.format.DateTimeFormatter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDateTime;

public class Benutzerverwaltung {

	public static void main(String[] args) {
		
		/* Zum Testen der Delete-Funktion der BenutzerListe
		BenutzerListe benutzerListe = new BenutzerListe();
		benutzerListe.insert(new Benutzer("A", "a"));
		benutzerListe.insert(new Benutzer("B", "b"));
		benutzerListe.insert(new Benutzer("C", "c"));
		benutzerListe.insert(new Benutzer("D", "d"));
		
		System.out.println("1) \n" + benutzerListe.select());
		benutzerListe.delete("B");
		System.out.println("2) \n" + benutzerListe.select());
		benutzerListe.delete("D");
		System.out.println("3) \n" + benutzerListe.select());
		benutzerListe.delete("A");
		System.out.println("4) \n" + benutzerListe.select());
		benutzerListe.delete("C");
		System.out.println("5) \n" + benutzerListe.select());
		 */
		
		try {
            BenutzerverwaltungV10.start();
        } catch (Exception e){
        	System.out.println(e);
            System.out.println("Die Benutzerverwaltung konnte nicht durchgeführt werden.\nBitte starten Sie das Programm erneut.");
        };
	}
}

class BenutzerverwaltungV10 {
	
    public static void start() throws Exception {
    	
    	Boolean binarySwitch = false;
    	Boolean serializableSwitch = true;
    	
        BenutzerListe benutzerListe = new BenutzerListe();

        //benutzerListe.insert(new Benutzer("Pazula", Crypto.encrypt("paula".toCharArray())));
        //benutzerListe.insert(new Benutzer("Adam37", Crypto.encrypt("adam37".toCharArray())));
        //benutzerListe.insert(new Benutzer("Darko", Crypto.encrypt("darko".toCharArray())));
        
        //Momentan wird encryptViginere genutzt!
        benutzerListe.insert(new Benutzer("Pazula", Crypto.encryptViginere("paula".toCharArray())));
        benutzerListe.insert(new Benutzer("Adam37", Crypto.encryptViginere("adam37".toCharArray())));
        benutzerListe.insert(new Benutzer("Darko", Crypto.encryptViginere("darko".toCharArray())));
        
        if (binarySwitch) {
        	//Binäre Datei einlesen
        	File dir = new File(benutzerListe.binaryFile);
        	if (!dir.exists()) {
        		System.out.println(benutzerListe.binaryFile + " nicht vorhanden.\nBenutzer konnten nicht geladen werden.");
        	} else {
        		ObjectInputStream objInpStream = new ObjectInputStream(new FileInputStream(benutzerListe.binaryFile));
                benutzerListe = (BenutzerListe)objInpStream.readObject();
                objInpStream.close();
        	}
        }
        
        if (serializableSwitch) {
        	//.txt Datei einlesen
        	File dir = new File(benutzerListe.serializableFile);
        	if (!dir.exists()) {
        		System.out.println(benutzerListe.serializableFile + " nicht vorhanden.\nBenutzer konnten nicht geladen werden.");
        	} else {
        		benutzerListe.dateiEinlesen();
        	}
        }

        System.out.println("\nAlle Nutzer:");
        System.out.println(benutzerListe.select());
        
        /* Zum testen der Enrcypt-Funktionen
        System.out.println(Crypto.encrypt("adam37".toCharArray()));
        System.out.println(Crypto.decrypt(Crypto.encrypt("adam37".toCharArray())));
        System.out.println(Crypto.encryptViginere("adam37".toCharArray()));
        System.out.println(Crypto.decryptViginere(Crypto.encryptViginere("adam37".toCharArray())));
		*/
        
        // Hier bitte das Menü mit der Auswahl
        //  - Anmelden
        //  - Registrieren
        // einfügen, sowie die entsprechenden Abläufe:
        // Beim Registrieren 2x das Passwort einlesen und vergleichen,
        // das neue Benutzerobjekt erzeugen und in die Liste einfügen.
        // Beim Anmelden (max. 3 Versuche) name und passwort einlesen,
        // in der Liste nach dem Namen suchen und das eingegebene Passwort
        // mit dem gespeicherten vergleichen.
        
        Scanner scanner = new Scanner(System.in);
        
        boolean loggedIn = false;
        
        while(!loggedIn) {
        	System.out.println("Menu:");
            System.out.println("1) Anmelden");
            System.out.println("2) Registrieren");
            System.out.println("3) Exit");
            
            System.out.print("\nChoose your option: ");
            int option = scanner.nextInt();
            
        	if (option == 1) {
        		//Anmelden
        		
        		boolean nameIsCorrect = false;
        		int attempts = 0;
        		
        		while (!nameIsCorrect & attempts < 3) {
        			System.out.print("\nBitte geben Sie ihren Benutzernamen ein: ");
                	String benutzerName = scanner.next();
                	
                	if(benutzerListe.select(benutzerName) == "") {
                		System.out.println("\nEs gibt keinen Benutzer mit dem Namen " + benutzerName);
                		
                		if (attempts < 2) {
                			System.out.println("Versuchen Sie es erneut. Sie haben noch " + (2 - attempts) + " Versuche.");
                		} else {
                			System.out.println("Sie haben ihre 3 Versuche verbraucht.");
                			System.exit(0);
                		}
                		 
                	} else {
                		Benutzer b = benutzerListe.selectBenutzer(benutzerName);
             			
                		nameIsCorrect = true;
                		boolean passwortIsCorrect = false;
                		attempts = 0;
                		
                		while (!passwortIsCorrect & attempts < 3) {
                			System.out.print("Willkommen " + benutzerName + ", bitte geben Sie Ihr Passwort ein: ");
                    		String passwort = scanner.next();
                			
                			if (b.hasPassword(Crypto.encryptViginere(passwort.toCharArray()))) {
                				System.out.println("\nSie sind angemeldet!");
                				passwortIsCorrect = true;
                				
                				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
                				LocalDateTime now = LocalDateTime.now();
                				b.setLastLogin(dtf.format(now));
                				System.out.println("Last login: " + b.getLastLogin());
                				
                				b.setStatus("Online");
                				loggedIn = true;
                				
                			} else {
                				System.out.println("\nFalsches Passwort für Nutzer " + benutzerName);
                        		
                        		if (attempts < 2) {
                        			System.out.println("Versuchen Sie es erneut. Sie haben noch " + (2 - attempts) + " Versuche.");
                        		} else {
                        			System.out.println("Sie haben ihre 3 Versuche verbraucht.");
                        			System.exit(0);
                        		}
                			}
                		}
                	}
                	attempts++;
        		}
            } else if (option == 2) {
            	//registrieren
            	
            	boolean successfulRegistration = false;
            	
            	while (!successfulRegistration) {
            		
            		System.out.print("Wahl des Nutzernamens: ");
            		String nutzerName = scanner.next();
            		
            		Benutzer b = benutzerListe.selectBenutzer(nutzerName);
            		
            		if (b == null) {
            			
            			boolean passwortSet = false;
            			while (!passwortSet) {
            				System.out.print("\nWahl des Passworts: ");
            				String firstPasswort = scanner.next();
            				System.out.print("Passwort erneut eingeben: ");
            				String secondPasswort = scanner.next();

            				if (firstPasswort.equals(secondPasswort)) {
            					System.out.println("\nSie haben sich erfolgreich registriert!\n");
            					
            					benutzerListe.insert(new Benutzer(nutzerName, Crypto.encryptViginere(firstPasswort.toCharArray())));
            					
            					successfulRegistration = true;
            					passwortSet = true;
            					
            					if (serializableSwitch) {
            						benutzerListe.dateiSchreiben();
            					}
            					
            					if (binarySwitch) {
            				        ObjectOutputStream objOutStream = new ObjectOutputStream(new FileOutputStream(benutzerListe.binaryFile));
            				        objOutStream.writeObject(benutzerListe);
            				        objOutStream.close();
            					}
            					
            				} else {
            					System.out.println("Deine Passwörter stimmen nicht überein. Geben Sie erneut ein Passwort ein.");
            				}
            			}
            			
            		} else {
            			System.out.println("Der Nutzername " + nutzerName + " wird bereits verwendet");
            		}
            	}
            	
            } else if (option == 3){
            	
            	if (serializableSwitch) {
					benutzerListe.dateiSchreiben();
				}
				
				if (binarySwitch) {
			        ObjectOutputStream objOutStream = new ObjectOutputStream(new FileOutputStream(benutzerListe.binaryFile));
			        objOutStream.writeObject(benutzerListe);
			        objOutStream.close();
				}
				
            	System.exit(0);
            } else {
            	System.out.println("Bitte wählen Sie eine der drei Optionen");
            }
        }
        
        //System.out.println("\nAlle Nutzer:");        
        //System.out.println(benutzerListe.select());
    }
}

class BenutzerListe implements Serializable {
	
	private static final long serialVersionUID = 2L;
	private Benutzer first;
    private Benutzer last;
    
    public String serializableFile = "Benutzer.txt";
    public String binaryFile = "Benutzer.bin";

    public BenutzerListe(){
        first = last = null;
    }
    
    public void insert(Benutzer b){
        // Sicherheitshalber setzen wir
        // den Nachfolger auf null:
        b.setNext(null);
        if(first == null){
            first = last = b;
        }
        else{
            last.setNext(b);
            last = b;
        }
    }
    public String select(){
        String s = "";
        Benutzer b = first;
        while(b != null){
            s += b.toString() + '\n';
            b = b.getNext();
        }
        return s;
    }
    public String select(String name){
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)){
                return b.toString();
            }
            b = b.getNext();
        }
        return "";
    }
    
    public Benutzer selectBenutzer(String name) {
    	Benutzer b = first;
    	while(b != null) {
    		if (b.hasName(name)) {
    			return b;
    		}
    		b = b.getNext();
    	}
    	return null;
    }

    public boolean delete(String name){
    	Benutzer nutzer = selectBenutzer(name);
    	
    	if (nutzer == null) {
        	return true;
        }
        
        if (nutzer == first) {
        	this.first = first.getNext();
        	return true;
        }
    	
    	Benutzer previous = first;
        Benutzer toDelete = first.getNext();
        
        while (toDelete != null) {
        	if (toDelete == nutzer) {
        		previous.setNext(toDelete.getNext());
        		toDelete.setNext(null);
        		return true;
        	}
        	
        	previous = toDelete;
        	toDelete = toDelete.getNext();
        }
        return true;
    }
    
    public Benutzer getFirst() {
    	return first;
    }
    
    public Benutzer getLast() {
    	return last;
    }

    public void dateiSchreiben() {
    	try {
    		String benutzerString;
    		FileWriter writer = new FileWriter("Benutzer.txt");
    		
    		if (this.first == null ) {
    			System.out.println("Keine Benutzer gespeichert.");
    		} else {
    			Benutzer b = this.first;
    			while (b != null) {
    				benutzerString = b.toString();
    				writer.write(benutzerString + System.lineSeparator());
    				b = b.getNext();
    			}
    		}
    		writer.close();
    		
    	} catch (IOException e) {
    		System.out.println("Datei konnte nicht geschrieben werden.");
    	}
    }
    
    public void dateiEinlesen() {
    	try {
    		String[] benutzer;
    		FileReader reader = new FileReader("Benutzer.txt");
    		BufferedReader inBuffer = new BufferedReader(reader);
    		
    		String line = inBuffer.readLine();
    		
    		while (line != null) {
    			benutzer = line.split(" ");
    			this.insert(new Benutzer(benutzer[0], benutzer[1].toCharArray()));
    			line = inBuffer.readLine();
    		}
    		
    		reader.close();
    	} catch (IOException e) {
    		System.out.println("Datei konnte nicht eingelesen werden.");
    	}	
    }
}

class Benutzer implements Serializable {

	private static final long serialVersionUID = 1L;
	private String name;
    private char[] passwort;

    private Benutzer next;
    
    private String status;
    private String lastLogin;

    public Benutzer(String name, char[] pw){
        this.name = name;
        this.passwort = pw;

        this.next = null;
    }

    public boolean hasName(String name){
        return name.equals(this.name);
    }
    
    public boolean hasPassword(char[] password) {
    	return Arrays.equals(passwort, this.passwort);
    }

    public String toString(){
        String s = "";
        s += name + " ";
        s += new String(this.passwort);
        return s;
    }

    public Benutzer getNext(){
        return next;
    }

    public void setNext(Benutzer b){
        next = b;
    }
    
    public String getStatus() {
    	return this.status;
    }
    
    public void setStatus(String status) {
    	this.status = status;
    }
    
    public String getLastLogin() {
    	return this.lastLogin;
    }
    
    public void setLastLogin(String loginDateAndTime) {
    	this.lastLogin = loginDateAndTime;
    }
}

class Crypto {

    private static int cryptoKey = 65500;
    private static char[] keyword = "Computer".toCharArray();

    public static char[] encrypt(char[] s) {
        char[] encrypted = new char[s.length];
        for(int i = 0; i < s.length; i++) {
            encrypted[i] = (char)(s[i] + cryptoKey % 128);
        }
        return encrypted;
    }
    
    public static char[] decrypt(char[] s) {
    	char[] decrypted = new char[s.length];
        for(int i = 0; i < s.length; i++) {
            decrypted[i] = (char)((s[i] + (128 - cryptoKey % 128)) % 128);
        }
        return decrypted;
    }
    
    public static char[] encryptViginere(char[] s) {
    	char[] encrypted = new char[s.length];
    	for (int i = 0; i < s.length; i++) {
    		encrypted[i] = (char)(s[i] + keyword[i % keyword.length] % 128);
    	}
    	return encrypted;
    }
    
    public static char[] decryptViginere(char[] s) {
    	char[] decrypted = new char[s.length];
    	for (int i = 0; i < s.length; i++) {
    		decrypted[i] = (char)((s[i] + 128 - keyword[i % keyword.length]) % 128);
    	}
    	return decrypted;
    }
}