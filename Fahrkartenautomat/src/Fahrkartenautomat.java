﻿//Claudia Grabowski
//12.01.2021
//FI-A-02
import java.util.Scanner;

class Fahrkartenautomat {
	
	static void warte(int millisekunden) {
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			
			try {
				Thread.sleep(millisekunden);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	     }
	}
	
	static double fahrkartenbestellungErfassen() {
		
		Scanner tastatur = new Scanner(System.in);
		
		String[] fahrkarten = {
		  "Kurzstrecke",
		  "Einzelfahrschein Berlin AB",
		  "Einzelfahrschein Berlin BC",
		  "Einzelfahrschein Berlin ABC",
		  "Tageskarte Berlin AB",
		  "Tageskarte Berlin BC",
		  "Tageskarte Berlin ABC",
		  "Kleingruppen-Tageskarte Berlin AB",
		  "Kleingruppen-Tageskarte Berlin BC",
		  "Kleingruppen-Tageskarte Berlin ABC"
		};
		
		double[] preise = {
		  1.9,
		  2.9,
		  3.3,
		  3.6,
		  8.6,
		  9.0,
		  9.6,
		  23.5,
		  24.3,
		  34.9
	    };
		
		Boolean ticketWahl = true;
		double preis = 0.0;
		
		System.out.println("Fahrkartenbestellvorgang:");
	    warte(250);
	    
	    while (ticketWahl) {
		   for (int i = 0; i < preise.length; i++) {
	    	  System.out.printf("\n[%2d] %-35s %.2f", i + 1, fahrkarten[i], preise[i]);
		   }
	      
	      System.out.print("\n\nWählen Sie: ");
	      int wahl = tastatur.nextInt() - 1;
	      
	      System.out.print("Anzahl der Tickets: ");
	      short anzahlKarten = tastatur.nextShort();
	      
	      while (anzahlKarten  < 1 || anzahlKarten > 10) {
	         System.out.print("Ungültige Eingabe:\nEs dürfen nicht weniger als eine und nicht mehr als 10 Karten gebucht werden.\n");
	         System.out.print("Anzahl der Tickets: ");
	         anzahlKarten = tastatur.nextShort();
	      }
	      
	      preis += anzahlKarten * preise[wahl];
	      
	      System.out.println("\n[ 1] Bezahlen");
	      System.out.print("[ 2] Mehr Tickets wählen");
	      System.out.print("\n\nWählen Sie: ");

	      
	      if (tastatur.nextInt() == 1) {
	    	  ticketWahl = false;
	      }
   		}
   		
   		return preis;
    }
   
   static double fahrkartenBezahlen(final double zuZahlenderBetrag) {
      
      Scanner tastatur = new Scanner(System.in);
      double eingezahlterGesamtbetrag = 0.0;
      
      while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
         System.out.printf("\nNoch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
         System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
         
         double eingeworfeneMünze = tastatur.nextDouble();
         eingezahlterGesamtbetrag += eingeworfeneMünze;
      }
      
      tastatur.close();
      
      return eingezahlterGesamtbetrag - zuZahlenderBetrag;
   }
   
   static void muenzeAusgeben(int betrag, String einheit) {
      System.out.println(betrag + " " + einheit);
   }
   
   static void rueckgeldAusgeben(double rückgabeBetrag) {
      if (rückgabeBetrag > 0.0) {
         System.out.printf("\n\nDer Rückgabebetrag in Höhe von %.2f EURO ", rückgabeBetrag);
         System.out.println("wird in folgenden Münzen ausgezahlt:");
         
         while(rückgabeBetrag >= 2.0) { // 2 EURO-Münzen
            muenzeAusgeben(2, "EURO");
            rückgabeBetrag -= 2.0;
         }
         while(rückgabeBetrag >= 1.0) { // 1 EURO-Münzen
            muenzeAusgeben(1, "EURO");
            rückgabeBetrag -= 1.0;
         }
         while(rückgabeBetrag >= 0.5) { // 50 CENT-Münzen
            muenzeAusgeben(50, "CENT");
            rückgabeBetrag -= 0.5;
         }
         while(rückgabeBetrag >= 0.2) { // 20 CENT-Münzen
            muenzeAusgeben(20, "CENT");
            rückgabeBetrag -= 0.2;
         }
         while(rückgabeBetrag >= 0.1) { // 10 CENT-Münzen
            muenzeAusgeben(10, "CENT");
            rückgabeBetrag -= 0.1;
         }
         while(rückgabeBetrag >= 0.05) { // 5 CENT-Münzen
            muenzeAusgeben(5, "CENT");
            rückgabeBetrag -= 0.05;
         }
       }
   }
   
   static void fahrkartenAusgeben() {
	   System.out.println("\nFahrschein wird ausgegeben /\nFahrscheine werden ausgegeben");
       warte(250);
       System.out.println("\nVergessen Sie nicht, den Fahrschein / ihre Fahrscheine\n"+
       "vor Fahrtantritt entwerten zu lassen!\n"+
       "Wir wünschen Ihnen eine gute Fahrt.");
   }
   
    public static void main(String[] args) {
      rueckgeldAusgeben(fahrkartenBezahlen(fahrkartenbestellungErfassen()));
      fahrkartenAusgeben();       
    }
}