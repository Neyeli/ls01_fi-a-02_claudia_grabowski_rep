import java.util.Scanner; //Für Aufgabe3


public class EinfacheUebungenArrays {
	
	public static void aufgabe1() {
		/* Aufgabe 1
		 * Schreiben Sie ein Programm „Zahlen“, in welchem ein 
		 * ganzzahliges Array der Länge 10 deklariert wird.
		 * Anschließend wird das Array mittels Schleife mit den 
		 * Zahlen von 0 bis 9 gefüllt. 
		 * Zum Schluss geben Sie die Elemente des Arrays wiederum 
		 * mit einer Schleife auf der Konsole aus.
		 */
		
		int[] zahlen = new int[10];
		
		for (int i = 0; i < zahlen.length; i++) {
			zahlen[i] = i;
		}
		
		System.out.println("Aufgabe 1:");
		for (int i = 0; i < zahlen.length; i++) {
			System.out.print(zahlen[i]);
			
			if (i < zahlen.length - 1) {
				System.out.print(", ");
			} else {
				System.out.print("\n");
			}
		}
	}
	
	public static void aufgabe2() {
		/* Aufgabe 2
		 * Das zu schreibende Programm „UngeradeZahlen“ ist 
		 * ähnlich der Aufgabe 1.
		 * Sie deklarieren wiederum ein Array mit 10 Ganzzahlen. 
		 * Danach füllen Sie es mit den ungeraden Zahlen von 
		 * 1 bis 19 und geben den Inhalt des Arrays über die 
		 * Konsole aus (Verwenden Sie Schleifen!).
		 */
		
		int[] zahlen = new int[10];
		
		for (int i = 0; i < zahlen.length; i++) {
			zahlen[i] = i * 2 + 1;
		}
		
		System.out.println("Aufgabe 2:");
		for (int i = 0; i < zahlen.length; i++) {
			System.out.print(zahlen[i]);
			
			if (i < zahlen.length - 1) {
				System.out.print(", ");
			} else {
				System.out.print("\n");
			}
		}
	}
	
	public static void aufgabe3() {
		/* Aufgabe 3
		 * Im Programm „Palindrom“ werden über die Tastatur 
		 * 5 Zeichen eingelesen und in einem geeigneten Array gespeichert. 
		 * Ist dies geschehen, wird der Arrayinhalt in umgekehrter
		 * Reihenfolge (also von hinten nach vorn) auf der Konsole ausgegeben.
		 */
		
		Scanner tastatur = new Scanner(System.in);		
		
		int[] zahlen = new int[5];
		
		System.out.println("Aufgabe 3:");
		for (int i = 0; i < zahlen.length; i++) {
			System.out.print("Bitte geben Sie eine Zahl ein: ");
			zahlen[i] = tastatur.nextInt();
		}
		
		for (int i = zahlen.length - 1; i >= 0; i--) {
			System.out.print(zahlen[i]);
			
			if (i > 0) {
				System.out.print(", ");
			} else {
				System.out.print("\n");
			}
		}
		
		tastatur.close();
	}
	
	public static void aufgabe4() {
		/* Aufgabe 4
		 * Jetzt wird Lotto gespielt. In der Klasse „Lotto“ 
		 * gibt es ein ganzzahliges Array, welches 6 Lottozahlen
		 * von 1 bis 49 aufnehmen kann. Konkret sind das die Zahlen 
		 * 3, 7, 12, 18, 37 und 42. 
		 * Tragen Sie diese im Quellcode fest ein.
		 */
		
		Scanner tastatur = new Scanner(System.in);		
		
		int[] zahlen = {3, 7, 12, 18, 37, 42};
		
		// a) Geben Sie zunächst schleifenbasiert den Inhalt des 
		// Arrays in folgender Form aus: [ 3 7 12 18 37 42 ]
		
		System.out.println("Aufgabe 4:");
		System.out.print("a) [");
		for (int i = 0; i < zahlen.length; i++) {
			
			System.out.print(" " + zahlen[i]);
			
			if (i == zahlen.length - 1) {
				System.out.print(" ");
			}
		}
		System.out.println("]");
		
		// b) Prüfen Sie nun nacheinander, ob die Zahlen 12 bzw. 13 
		// in der Lottoziehung vorkommen.Geben Sie nach der Prüfung aus:
		// "Die Zahl 12 ist in der Ziehung enthalten."
		// "Die Zahl 13 ist nicht in der Ziehung enthalten."
		
		boolean exists_12 = false;
		boolean exists_13 = false;
		
		for (int i = 0; i < zahlen.length; i++) {
			if (zahlen[i] == 12) {
				exists_12 = true;
			}
			
			if (zahlen[i] == 13) {
				exists_13 = true;
			}
		}
		
		System.out.println("b)");
		if (exists_12) {
			System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
		} else {
			System.out.println("Die Zahl 12 ist nicht in der Ziehung enthalten.");
		}
		
		if (exists_13) {
			System.out.println("Die Zahl 13 ist in der Ziehung enthalten.");
		} else {
			System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
		}
		
		tastatur.close();
	}

	public static void main(String[] args) {		
		aufgabe1();
		aufgabe2();
		aufgabe3();
		aufgabe4();
	}

}
