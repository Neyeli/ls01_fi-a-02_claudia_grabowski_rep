
public class Fakultaet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		char equal = '=';
		String equation = "";
		int result = 1;
		
		for (int num = 0; num <= 5; num++) {
			//Musste es hier rechtsbuendig machen wegen dem Ausrufezeichen
			System.out.printf("%d!%5c ", num, equal);
			
			if (num > 1) {
				equation += " * ";
			}
			
			if (num > 0) {
				equation += num;
				result *= num;
			}
		
			System.out.printf("%-19s", equation);
			System.out.printf("%c", equal);
			System.out.printf("%4d\n", result);
		}
	}

}
